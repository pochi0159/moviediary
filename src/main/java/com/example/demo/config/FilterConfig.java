package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.LoginFilter;

@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean<LoginFilter> loginFilter() {

		FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());

		bean.addUrlPatterns("/");
		/*bean.addUrlPatterns("/signup");*/
		bean.addUrlPatterns("/edit");
		bean.addUrlPatterns("/detail");
		bean.addUrlPatterns("/review");
		bean.setOrder(1);

		return bean;


	}
}
