package com.example.demo.specification;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.example.demo.entity.Movie;


public class MovieSpecifications {
	    public static Specification<Movie> categoryContains(final String category) {
	        return StringUtils.isEmpty(category) ? null : new Specification<Movie>() {
	            @Override
	            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
	                return cb.like(root.get("category"), "%" + category + "%");
	            }
	        };
	    }
	    public static Specification<Movie> titleIn(final List<String> titles) {
	        return titles==null||titles.size()==0 ? null : new Specification<Movie>() {
	            @Override
	            public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
	                return cb.in(root.get("title")).value(titles);
	            }
	        };
	    }
	}