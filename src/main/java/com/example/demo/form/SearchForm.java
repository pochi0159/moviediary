package com.example.demo.form;

import lombok.Data;

@Data
public class SearchForm {
    private int page;
    private String title1="";
    private String title2="";
    private String title3="";
    private String category="";
}