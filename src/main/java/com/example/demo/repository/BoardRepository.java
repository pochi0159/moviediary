package com.example.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Movie;

@Repository
public class BoardRepository {

    @PersistenceContext
    private EntityManager em;

    public int findAllCnt() {
        return ((Number) em.createQuery("select count(*) from Movie")
                    .getSingleResult()).intValue();
    }

    public List<Movie> findListPaging(int startIndex, int pageSize) {
        return em.createQuery("select b from Movie b", Movie.class)
                    .setFirstResult(startIndex)
                    .setMaxResults(pageSize)
                    .getResultList();
    }

    public List<Movie> findListPage(int startIndex, int pageSize, String title) {
    return em.createQuery("select b from Movie b ORDER BY b.createdDate DESC", Movie.class)
                .setFirstResult(startIndex)
                .setMaxResults(pageSize)
                .getResultList();
    }
}