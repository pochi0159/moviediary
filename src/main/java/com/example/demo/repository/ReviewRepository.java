package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Movie;

@Repository
public interface ReviewRepository extends JpaRepository<Movie, Integer> {


	@Query(value= "SELECT * FROM movies INNER JOIN users ON movies.user_id = users.id  ", nativeQuery = true)
	List<Movie> findAll();

	Movie findById(int id);

	/*public Page<Movie> findAll(Pageable pageable);*/
}
