package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Movie;
import com.example.demo.repository.ReviewRepository;


@Service
@Transactional
public class ReviewService {

	@Autowired
	ReviewRepository reviewRepository;

	public void saveReview(Movie movie) {
		reviewRepository.save(movie);
	}

	/*public List<Movie> findAllMovie(String title, String category, int rate) {
		return reviewRepository.findAll();
	}*/

	public Movie findById(int id) {
		return reviewRepository.findById(id);
	}

	public void deleteDetail(int id) {
		reviewRepository.deleteById(id);
	}
	public Page<Movie> getAllWord(Pageable pageable) {

        return reviewRepository.findAll(pageable);
    }

	}
