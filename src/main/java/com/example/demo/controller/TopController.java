package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Movie;
import com.example.demo.entity.User;
import com.example.demo.pagination.Pagination;
import com.example.demo.repository.BoardRepository;
import com.example.demo.service.ReviewService;

@Controller
public class TopController {

	@Autowired
	BoardRepository boardRepository;

	@Autowired
	ReviewService reviewService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;


	@GetMapping("/")
	ModelAndView top(Model model, @RequestParam(defaultValue = "1") int page) {


		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String rate = request.getParameter("rate");
		ModelAndView mav = new ModelAndView();
		User loginUser =(User) request.getSession().getAttribute("loginUser");
		Object errorMessages = session.getAttribute("errorMessages");

		mav.addObject("loginUser", loginUser);
		mav.addObject("errorMessages", errorMessages);
		mav.addObject("title", title);
		mav.addObject("category", category);
		mav.addObject("rate", rate);

		mav.setViewName("index");

		// 総掲示物数
	    int totalListCnt = boardRepository.findAllCnt();

	    // 総掲示物数と現在ページ
	    Pagination pagination = new Pagination(totalListCnt, page);

	    // DB接近スタートインデックス
	    int startIndex = pagination.getStartIndex();

	    // ページことに表示する掲示物最大数
	    int pageSize = pagination.getPageSize();
	   /* int pageSize = 5;*/

	    // 掲示物取得
	    List<Movie> boardList = boardRepository.findListPaging(startIndex, pageSize);

	 /*   List<Movie> movies = reviewService,findAllMovie(title, category, rate);
*/
	    // モデルオブジェクトにオブジェクト格納
	    mav.addObject("movies", boardList);
	    mav.addObject("pagination", pagination);

		return mav;
	}



	@GetMapping("/logout")
	public ModelAndView logout() {

		session.invalidate();
		return new ModelAndView("forward:/top");
	}

}
