package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Movie;
import com.example.demo.entity.User;
import com.example.demo.service.ReviewService;

@Controller
public class EditController {

	@Autowired
	ReviewService reviewService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;

	@GetMapping("/edit/{id}")
	public ModelAndView message(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		session = request.getSession();
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.addObject("movie", reviewService.findById(id));
		mav.addObject("rate", reviewService.findById(id).getRate());
		mav.setViewName("/edit");
		return mav;
	}

	@PutMapping("/edit/{id}")
	public ModelAndView addMessage(@PathVariable Integer id, @ModelAttribute("Movie") Movie movie) {
		User user = (User) request.getSession().getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();


		if(!isValid(errorMessages, movie)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("movie", movie);
			mav.setViewName("/edit");
			return mav;
		}
		Date date = new Date();
		movie.setUpdatedDate(date);
		movie.setRate(movie.getRate());
		movie.setUserId(user.getId());
		reviewService.saveReview(movie);
		return new ModelAndView("redirect:/");
	}

	public boolean isValid(List<String> errorMessages, Movie movie) {

		if(!StringUtils.isBlank(movie.getTitle())&& movie.getTitle().length() > 30) {
			errorMessages.add("題名は30文字以内で入力してください。");
		}
		if(!StringUtils.isBlank(movie.getCategory())&& movie.getCategory().length() > 10) {
			errorMessages.add("カテゴリーは10文字以内で入力してください。");
		}
		if(!StringUtils.isBlank(movie.getText()) &&movie.getText().length() > 1000) {
			errorMessages.add("感想は1000字以内で入力してください。");
		}
		if(StringUtils.isBlank(movie.getTitle())) {
				errorMessages.add("タイトルを入力してください。");
		}
		if(StringUtils.isBlank(movie.getCategory())) {
			errorMessages.add("カテゴリーを入力してください。");
		}
		if(StringUtils.isBlank(movie.getText())) {
			errorMessages.add("感想を入力してください。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}
