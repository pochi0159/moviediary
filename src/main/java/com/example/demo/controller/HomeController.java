package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;


@Controller
public class HomeController {

	@Autowired
	HttpServletRequest request;


	@GetMapping("/top")
	ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		User loginUser =(User) request.getSession().getAttribute("loginUser");

		mav.addObject("loginUser", loginUser);
		mav.setViewName("/top");
		return mav;
	}
}