package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.entity.Movie;
import com.example.demo.entity.User;
import com.example.demo.service.ReviewService;

@Controller
public class ReviewController {

	@Autowired
	ReviewService reviewService;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpSession session;

	@GetMapping("/review")
	public ModelAndView message() {
		ModelAndView mav = new ModelAndView();
		session = request.getSession();
		mav.addObject("errorMessages", session.getAttribute("errorMessages"));
		mav.addObject("movieForm", new Movie());
		mav.setViewName("/review");
		return mav;
	}

	@PostMapping("/review")
	public ModelAndView addMessage(RedirectAttributes redirectAttributes,
			@ModelAttribute("movieForm") Movie movie) {

		User user = (User) request.getSession().getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();
		String rate = request.getParameter("rate");


		if(!isValid(errorMessages, movie, rate)) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("movie", movie);
			mav.setViewName("/review");
			return mav;
		}

		movie.setRate(Integer.parseInt(rate));
		movie.setUserId(user.getId());
		reviewService.saveReview(movie);
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/detail/{id}")
	public ModelAndView showDetail(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Movie movie = reviewService.findById(id);
		mav.addObject("movie", movie);
		mav.addObject("loginUser", (User) request.getSession().getAttribute("loginUser"));
		mav.setViewName("/detail");

		return mav;
	}

	public boolean isValid(List<String> errorMessages, Movie movie, String rate) {

		if(!StringUtils.isBlank(movie.getTitle())
				&& movie.getTitle().length() > 30) {
			errorMessages.add("タイトルは30文字以内で入力してください。");
		}
		if(!StringUtils.isBlank(movie.getCategory())
				&& movie.getCategory().length() > 10) {
			errorMessages.add("カテゴリーは10文字以内で入力してください。");
		}
		if(!StringUtils.isBlank(movie.getText()) &&
				movie.getText().length() > 1000) {
			errorMessages.add("本文は1000字以内で入力してください。");
		}
		if(StringUtils.isBlank(movie.getTitle())) {
			errorMessages.add("タイトルを入力してください。");
		}
		if(StringUtils.isBlank(movie.getCategory())) {
			errorMessages.add("カテゴリーを入力してください。");
		}
		if(StringUtils.isBlank(movie.getText())) {
			errorMessages.add("本文を入力してください。");
		}
		if(StringUtils.isBlank(rate)) {
			errorMessages.add("評価を入力してください。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

	@PostMapping("/delete/{id}")
	public ModelAndView deleteDetail(@PathVariable Integer id) {
		reviewService.deleteDetail(id);
		return new ModelAndView("redirect:/");
	}
}
