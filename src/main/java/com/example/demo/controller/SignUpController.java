package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.form.UserForm;
import com.example.demo.service.UserService;

@Controller
public class SignUpController {

	@Autowired
	UserService userService;

	@GetMapping("/signup")
	public ModelAndView getSignup() {
		ModelAndView mav = new ModelAndView();
		User user = new User();
		mav.setViewName("/signup");
		mav.addObject("userForm", user);
		return mav;
	}

	@PostMapping("/signup")
	public ModelAndView addUser(UserForm userForm) {

		User user = new User();
		user.setAccount(userForm.getAccount());
		user.setPassword(userForm.getPassword());
		user.setEmail(userForm.getEmail());

		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(errorMessages, user, userForm.getCheckPassword())) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			mav.addObject("userForm", user);
			mav.setViewName("/signup");
			return mav;
		}

		userService.saveUser(user);
		return new ModelAndView("redirect:/");
	}

	public boolean isValid(List<String> errorMessages, User user, String enteredPassword) {
		String account = user.getAccount();
		String password = user.getPassword();
		String checkPassword = enteredPassword;

		User userAlreadyUsed = userService.findByAccount(account);

		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウントが入力されていません。");
		}
		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません。");
		}
		if(!StringUtils.isBlank(account) && account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(account) && account.length() < 6 ) {
			errorMessages.add("アカウントは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(account) && (!account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントに使用できる文字は半角英数字のみです。");
		}
		if(!StringUtils.isBlank(password) && password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(password) && password.length() < 6 ) {
			errorMessages.add("パスワードは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(password) && (!password.matches("^[\\p{Alnum}|\\p{Punct}]+$"))) {
			errorMessages.add("パスワードに使用できる文字は半角英数字と記号のみです。");
		}
		if(!StringUtils.isBlank(password) && !password.equals(checkPassword)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません。");
		}
		if(userAlreadyUsed != null) {
			errorMessages.add("アカウントが重複しています。");
		}

		if(errorMessages.size() != 0){
			return false;
		}
		return true;
	}


}
